var p = new Parallax('.parallax').init();

(function ($) {
    var $masonry_grid = $('.blog__items-masonry').masonry({
        itemSelector: '.blog__item-masonry',
        columnWidth: '.masonry-sizer',
        percentPosition: true
    });

    $masonry_grid.imagesLoaded().progress(function () {
        $masonry_grid.masonry('layout');
    });

    $('.articles-readmore').readmore({
        speed: 500,
        collapsedHeight: 380,
        moreLink: '<a href="#" class="btn btn-light btn-readmore">Read more</a>',
        lessLink: '<a href="#" class="btn btn-light btn-readmore">Read less</a>',
        afterToggle: function (trigger, element, expanded) {
            if (!expanded) { // The "Close" link was clicked
                $('html, body').animate({scrollTop: element.offset().top}, {duration: 100});
            }
        }
    });

    $(".owl-carousel").owlCarousel({
        animateOut: 'fadeOut',
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 5000,
        // stagePadding: 30,
        smartSpeed: 450
    });

    $(function () {
        $('a.scroll').bind('click', function (event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 800);
            event.preventDefault();
        });
        $('a.nav-link').bind('click', function (event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 800);
            event.preventDefault();
        });
    });

    var $arrow = $('img.animated__arrow');

    $arrow.css('opacity', 0);
    var waypoints = $arrow.waypoint(function () {
        console.log('here!');
        $arrow.addClass('fadeInLeftBig');
    }, {offset: '80%'});

    if ($('#myCanvasContainer').length > 0) {

        if (typeof TagCanvas.Start !== 'undefined' && $.isFunction(TagCanvas.Start)) {
            TagCanvas.Start('myCanvas', 'tags', {
                textColour: '#999',
                outlineColour: '#999',
                textHeight: 26,
                shuffleTags: true,
                stretchX: 3,
                reverse: true,
                depth: 0.8,
                minSpeed: 0.01,
                maxSpeed: 0.05,
                wheelZoom: false,
                weight: true,
                weightMode: 'both'
            });
        }
        if (typeof TagCanvas.SetSpeed !== 'undefined' && $.isFunction(TagCanvas.SetSpeed)) {
            TagCanvas.SetSpeed('myCanvas', [0.01, -0.25]);
        }
    }

    setInterval(function () {
        $('.callback').show();
    }, 20000);

    $(document).ready(function(){
        $(window).scroll(function(){
            if($(this).scrollTop() > 600){
                $('#back-to-top').fadeIn();
            }else{
                $('#back-to-top').fadeOut();
            }
        });
        $('#back-to-top').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
    });

}(jQuery));