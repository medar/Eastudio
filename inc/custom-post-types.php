<?php
function register_custom_types() {
	register_post_type( 'remodel', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'Remodels',
			'singular_name'      => 'Remodel',
			'add_new'            => 'Add remodel',
			'add_new_item'       => 'Add remodel',
			'edit_item'          => 'Edit remodel',
			'new_item'           => 'New remodel',
			'view_item'          => 'View remodel',
			'search_items'       => 'Search remodel',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Remodels',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-hammer',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
	register_post_type( 'portfolio', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'Portfolio',
			'singular_name'      => 'Portfolio',
			'add_new'            => 'Add Portfolio item',
			'add_new_item'       => 'Add Portfolio item',
			'edit_item'          => 'Edit Portfolio',
			'new_item'           => 'New portfolio',
			'view_item'          => 'View portfolio',
			'search_items'       => 'Search portfolio',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Portfolio',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-portfolio',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array('portfolio'),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );

}

function create_taxonomy(){
	register_taxonomy('portfolio_cat', array('portfolio'), array(
		'label'                 => '',
		'labels'                => array(
			'name'              => 'Portfolio category',
			'singular_name'     => 'Portfolio category',
			'search_items'      => 'Search Portfolio categories',
			'all_items'         => 'All Portfolio',
			'view_item '        => 'View Portfolio',
			'parent_item'       => 'Parent category',
			'parent_item_colon' => 'Parent category:',
			'edit_item'         => 'Edit Portfolio',
			'update_item'       => 'Update Portfolio',
			'add_new_item'      => 'Add New Portfolio category',
			'new_item_name'     => 'New Portfolio Name',
			'menu_name'         => 'Portfolio categories',
		),
		'description'           => '', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => true, // равен аргументу show_ui
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		'hierarchical'          => true,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
		'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		'_builtin'              => false,
		'show_in_quick_edit'    => null, // по умолчанию значение show_ui
	) );
}
add_action('init', 'create_taxonomy');
add_action( 'init', 'register_custom_types' );