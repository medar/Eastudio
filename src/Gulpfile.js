var themename = 'eastudio';

var gulp = require('gulp'),
    autoprefixer = require('autoprefixer'),
    browserSync = require('browser-sync').create(),
    image = require('gulp-image'),
    jshint = require('gulp-jshint'),
    postcss = require('gulp-postcss'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),

    // Only work with new or updated files
    newer = require('gulp-newer'),

    // Name of working theme folder
    root = '../',
    scss = 'sass/style.scss',
    js = root + 'js/',
    img = root + 'img/';

gulp.task('browser-sync', function () {
    browserSync.init({
        open: 'external',
        proxy: 'extrarchstudio.dev',
        port: 8080,
        // tunnel: true,
    });
});

// CSS via Sass and Autoprefixer
gulp.task('css', function () {
    return gulp.src(scss)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            indentType: 'tab',
            indentWidth: '1'
        }).on('error', sass.logError))
        .pipe(postcss([
            autoprefixer('last 2 versions', '> 1%')
        ]))
        .pipe(sourcemaps.write(''))
        .pipe(gulp.dest(root))
        .pipe(browserSync.stream({match: '**/*.css'}));
});

// Optimize images through gulp-image
gulp.task('images', function () {
    return gulp.src(img + 'RAW/**/*.{jpg,JPG,png}')
        .pipe(newer(img))
        .pipe(image())
        .pipe(gulp.dest(img));
});

// JavaScript
gulp.task('javascript', function () {
    return gulp.src([js + '*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(gulp.dest(js));
});

// Watch everything
gulp.task('watch', ['css', 'browser-sync'], function () {
    gulp.watch([root + 'src/sass/**/*.scss'], ['css']);
    // gulp.watch(js + '**/*.js', ['javascript']);
    // gulp.watch(img + 'RAW/**/*.{jpg,JPG,png}', ['images']);
    gulp.watch([root + '**/*.php', root + 'js/theme.js'], browserSync.reload);
});

// Default task (runs at initiation: gulp --verbose)
gulp.task('default', ['watch']);