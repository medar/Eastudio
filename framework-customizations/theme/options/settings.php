<?php
if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * Framework options
 *
 * @var array $options Fill this array with options to generate framework settings from in backend
 */
$options = array(
	'general' => array(
		'title'   => 'General',
		'type'    => 'tab',
		'options' => array(
			'slider-box' => array(
				'title' => 'Слайдер',
				'type' => 'box',
				'options' => array(
					'slider-img' => array(
						'label' => 'Изображения слайдера',
						'type' => 'multi-upload',
						'images_only' => true,
					)
				)
			),
			'footer-box'           => array(
				'title'   => 'Футер',
				'type'    => 'box',
				'options' => array(
					'footer' => array(
						'label' => 'Footer',
						'type'  => 'wp-editor',
						'desc'  => "Контент для футера сайта",
						'size' => 'large', // small, large
						'editor_height' => 300,
						'wpautop' => true,
						'editor_type' => 'tinymce', // tinymce, html
					)
				)
			),
			'social-links-box'      => array(
				'title'   => 'Social links',
				'type'    => 'box',
				'options' => array(
					'social_fb' => array(
						'label' => 'Facebook',
						'type'  => 'text',
					),
					'social_vk' => array(
						'label' => 'Vkontakte',
						'type'  => 'text',
					),
					'social_tw' => array(
						'label' => 'Twitter',
						'type'  => 'text',
					),
					'social_gp' => array(
						'label' => 'G+',
						'type'  => 'text',
					),
					'social_in' => array(
						'label' => 'LinkedIn',
						'type'  => 'text',
					),
				)
			),
		)
	)
);