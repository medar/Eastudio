<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}


?>
<div class="fw-accordion">
	<?php foreach ( fw_akg( 'tabs', $atts, array() ) as $tab ) : ?>
		<?php
		$is_list = false;
		if ( isset($tab['list']) ) {
			$is_list = $tab['list'];
		}
		$tab_color = '';
		if ( ! empty( $tab['tab_color'] ) ) {
			$tab_color = 'background-color:' . $tab['tab_color'] . ';';
		}
		$section_style = ( $tab_color ) ? 'style="' . esc_attr( $tab_color ) . '"' : '';
		if ( $is_list ) :
			$content      = trim( $tab['tab_content'], PHP_EOL );
			$content_list = explode( PHP_EOL, $content );
		endif;
		?>
		<h3 class="fw-accordion-title" <?php echo $section_style; ?>><?php echo $tab['tab_title']; ?></h3>
		<div class="fw-accordion-content">
			<?php if ( $is_list ) : ?>
				<ul class="accordion__services-list">
					<?php foreach ( $content_list as $item ) : ?>
						<li><?php echo $item; ?></li>
					<?php endforeach; ?>
				</ul>
			<?php else : ?>
				<?php echo do_shortcode( $tab['tab_content'] ); ?>
			<?php endif; ?>

		</div>
	<?php endforeach; ?>
</div>