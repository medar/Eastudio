<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$class = fw_ext_builder_get_item_width( 'page-builder', $atts['width'] . '/frontend_class' );

$pl = '';
if ( '' !== $atts['padding_left'] ) {
	$pl = 'padding-left:' . $atts['padding_left'] . ';';
}

$pr = '';
if ( '' !== $atts['padding_right'] ) {
	$pr = 'padding-right:' . $atts['padding_right'] . ';';
}

$pt = '';
if ( '' !== $atts['padding_top'] ) {
	$pt = 'padding-top:' . $atts['padding_top'] . ';';
}

$pb = '';
if ( '' !== $atts['padding_bottom'] ) {
	$pb = 'padding-bottom:' . $atts['padding_bottom'] . ';';
}

$column_style = ( $pl || $pr || $pt || $pb ) ? 'style="' . esc_attr( $pl . $pr . $pt . $pb ) . '"' : '';

?>
<div class="<?php echo esc_attr( $class ); ?>" <?php echo $column_style; ?>>
	<?php echo do_shortcode( $content ); ?>
</div>
