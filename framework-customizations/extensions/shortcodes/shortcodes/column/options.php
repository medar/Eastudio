<?php if (!defined('FW')) {
	die('Forbidden');
}

$options = array(
	'padding_left' => array(
		'label' => "Отступ слева",
		'type'  => 'text',
	),
	'padding_right' => array(
		'label' => "Отступ справа",
		'type'  => 'text',
	),
	'padding_top' => array(
		'label' => "Отступ сверху",
		'type'  => 'text',
	),
	'padding_bottom' => array(
		'label' => "Отступ снизу",
		'type'  => 'text',
	)
);
