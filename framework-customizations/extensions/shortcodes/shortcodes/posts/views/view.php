<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

?>

<div class="row">
	<?php foreach ( fw_akg( 'tabs', $atts, array() ) as $post ) :
		$post_item_id = $post['tab_title'];
		$post_item = get_post( $post_item_id );
		$image_id = get_post_thumbnail_id( $post_item_id );
		?>
		<div class="col-6 fw-post-item">
			<a href="<?php echo get_permalink( $post_item_id ) ?>">
				<picture>
					<?php echo wp_get_attachment_image( $image_id, array( 300, 300 ) ); ?>
				</picture>
			</a>
			<h3><a href="<?php echo get_permalink( $post_item_id ) ?>"><?php echo $post_item->post_title; ?></a></h3>
		</div>

	<?php endforeach; ?>

</div>
