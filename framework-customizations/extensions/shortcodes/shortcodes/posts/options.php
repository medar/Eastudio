<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

function eastudio_get_posts() {
	$args = array(
		'numberposts'    => - 1,
		'post_status'    => 'publish',
		'orderby'        => 'title',
		'order'          => 'ASC',
		'posts_per_page' => - 1,
		'post_type'      => 'post',
	);

	$posts = get_posts( $args );

	return $posts;
}

function eastudio_posts_choices( $posts ) {
	$choices = array();

	foreach ( $posts as $post ) :
		$choices[ $post->ID ] = $post->post_title;
	endforeach;

	return $choices;
}

$options = array(
	'tabs' => array(
		'type'          => 'addable-popup',
		'label'         => 'Пост',
		'popup-title'   => 'Добавить',
		'template'      => '{{=tab_title}}',
		'popup-options' => array(
			'tab_title' => array(
				'type'    => 'select',
				'label'   => 'Пост',
				'choices' => eastudio_posts_choices( eastudio_get_posts() )
			),
		)
	)
);