<?php if (!defined('FW')) die('Forbidden');

$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/posts');
wp_enqueue_style(
	'fw-shortcode-posts',
	$uri .'/static/css/style.css'
);
