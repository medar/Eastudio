<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => 'Посты',
		'description' => 'Добавляет превью постов',
		'tab'         => 'Content Elements',
		'popup_size'  => 'medium',
	)
);
