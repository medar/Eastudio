<?php if (!defined('FW')) die('Forbidden');

$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/team-member');
wp_enqueue_style(
	'fw-shortcode-team-member',
	$uri .'/static/css/styles.css'
);
