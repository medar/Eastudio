<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

if ( empty( $atts['image'] ) ) {
	$image = fw_get_framework_directory_uri('/static/img/no-image.png');
} else {
	$image_id = $atts['image']['attachment_id'];
}
?>

<div class="team__item">
<!--	--><?php //var_dump($atts['image']); ?>
	<div class="team__picture--wrapper">
		<picture>
<!--			<img src="--><?php //echo esc_attr($image); ?><!--" alt="--><?php //echo esc_attr($atts['name']); ?><!--"/>-->
			<?php echo wp_get_attachment_image($image_id, 'full');?>
		</picture>
		<span class="team--overlay"></span>
		<span class="team--triangle"></span>
	</div>
	<div class="team__info">
		<span class="team__name"><?php echo $atts['name']; ?></span>
		<span class="team__subinfo"><?php echo $atts['job']; ?></span>
	</div>
</div>
