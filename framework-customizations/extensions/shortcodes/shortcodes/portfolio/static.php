<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$uri = fw_get_template_customizations_directory_uri( '/extensions/shortcodes/shortcodes/portfolio' );
wp_enqueue_style(
	'fw-shortcode-portfolio',
	$uri . '/static/css/styles.css'
);

wp_enqueue_script(
	'waypoints',
	'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js',
	array(  ),
	'4.0.1',
	true
);

wp_enqueue_script(
	'fw-shortcode-portfolio-js',
	$uri . '/static/js/script.js',
	array( 'imagesloaded-js', 'jquery', 'waypoints' ),
	'1.0.0',
	true
);





