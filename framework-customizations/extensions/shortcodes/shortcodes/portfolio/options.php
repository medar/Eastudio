<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'portfolio_item_count' => array(
		'label'   => 'Сколько элементов вывести',
		'type'    => 'text'
	),
);
