<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
?>


<div class="portfolio--masonry">
		<div class="grid-sizer col-sm-6 col-md-4 col-lg-3"></div>
		<?php eastudio_portfolio_items( 4 ); ?>
</div>
<a href="#" id="infinitescroll-on_scroll">Load more</a>
