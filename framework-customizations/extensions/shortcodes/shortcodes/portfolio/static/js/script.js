(function ($) {
// init Masonry
    var $grid = $('.portfolio--masonry').masonry({
        itemSelector: '.portfolio__item--masonry',
        columnWidth: '.grid-sizer',
        percentPosition: true
    });
// layout Masonry after each image loads
    $grid.imagesLoaded().progress(function () {
        $grid.masonry('layout');
    });

    function get_offset() {
        return $('.portfolio__item--masonry').length;
    }
    var hold = false;

    var waypoints = $('#infinitescroll-on_scroll').waypoint(function (direction) {
        if (!hold) {
            hold = true;
            var data = {
                action: 'infinite_scroll',
                offset: get_offset()
            };
            jQuery.post(ajaxurl, data, function (response) {
                var $new = $(response);
                $('.portfolio--masonry').append($new).masonry('appended', $new);
                Waypoint.refreshAll();
                hold = false;
            });
        }
    }, {
        offset: '90%'
    });


}(jQuery));