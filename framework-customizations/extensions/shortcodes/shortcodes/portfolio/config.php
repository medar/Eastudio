<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => 'Портфолио',
		'description' => 'Выводит ваши работы из портфолио',
		'tab' => 'Content Elements',
		'popup_size' => 'medium',
	)
);
