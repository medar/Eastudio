<?php if (!defined('FW')) {
	die('Forbidden');
}

$options = array(
	'is_fullwidth' => array(
		'label'        => 'На всю ширину',
		'type'         => 'switch',
	),
	'id' => array(
		'label' => 'ID секции',
		'help' => 'Нужно для создания якоря. Должен быть уникальным для каждого элемента!',
		'type' => 'text'
	),
	'class' => array(
		'label' => 'Дополнительные классы',
		'help' => 'Через пробел можно задать дополнительные классы',
		'type' => 'text'
	),
	'background_color' => array(
		'label' => 'Цвет фона',
		'type'  => 'color-picker',
	),
	'background_image' => array(
		'label'   => 'Фоновое изображение',
		'type'    => 'background-image',
		'choices' => array(//	in future may will set predefined images
		)
	),
	'video' => array(
		'label' => 'Фоновое видео',
		'desc'  => __('Insert Video URL to embed this video', 'fw'),
		'type'  => 'text',
	)
);
