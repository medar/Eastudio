<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}


function get_portfolio_items() {
	$args = array(
		'numberposts'    => - 1,
		'post_status'    => 'publish',
		'orderby'        => 'title',
		'order'          => 'ASC',
		'posts_per_page' => - 1,
		'post_type'      => 'portfolio',
	);

	$posts = get_posts( $args );

	return $posts;
}

function portfolio_choices( $posts ) {
	$choices = array();

	foreach ( $posts as $post ) :
		$choices[$post->ID] = $post->post_title;
	endforeach;

	return $choices;
}

$options = array(
	'portfolio_item' => array(
		'label'   => 'Элемент портфолио',
		'type'    => 'select',
		'choices' => portfolio_choices(get_portfolio_items())
	),
);
