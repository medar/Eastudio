<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$portfolio_item_id = $atts['portfolio_item'];
$portfolio_item    = get_post( $portfolio_item_id );

$image_id = get_post_thumbnail_id( $portfolio_item_id );

$portfolio_item_cats = get_the_terms( $portfolio_item, 'portfolio_cat' );

if ( ! function_exists( 'get_the_portfolio_terms' ) ) :

	function get_the_portfolio_terms( $terms = false ) {
		$ans = '';
		if ( $terms ) :
			foreach ( $terms as $term ) :
				$ans .= $term->name . '/';
			endforeach;
		endif;

		$ans = trim( $ans, '/' );

		return $ans;
	}

endif;

?>

<div class="portfolio__item">
	<div class="portfolio__picture--wrapper">
		<picture>
			<?php echo wp_get_attachment_image( $image_id, 'full' ); ?>
		</picture>
	</div>
	<div class="portfolio__info--wrapper">

		<a class="portfolio__link" href="<?php echo get_permalink($portfolio_item_id); ?>">
			<div class="portfolio__info">
				<h4><?php echo $portfolio_item->post_title; ?></h4>
				<p><?php echo get_the_portfolio_terms( $portfolio_item_cats ); ?></p>
			</div>
		</a>
	</div>
</div>

