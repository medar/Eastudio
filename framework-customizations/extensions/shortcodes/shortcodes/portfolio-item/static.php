<?php if (!defined('FW')) die('Forbidden');

$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/portfolio-item');
wp_enqueue_style(
	'fw-shortcode-portfolio-item',
	$uri .'/static/css/styles.css'
);
