<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => 'Элемент портфолио',
		'description' => 'Добавить один элемент портфолио',
		'tab' => 'Content Elements',
		'popup_size' => 'medium',
	)
);
