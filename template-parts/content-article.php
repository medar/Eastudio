<article id="post-<?php the_ID(); ?>" <?php post_class( "article__full" ); ?>>
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="title-img__wrapper">
			<picture>
				<?php the_post_thumbnail( 'full', array( 'class' => 'parallax' ) ); ?>
			</picture>
			<header class="article-header">
				<div class="container">
					<h1><?php the_title(); ?></h1>
				</div>
			</header>
		</div>
	<?php else : ?>
		<header class="article-header">
			<div class="container">
				<h1><?php the_title(); ?></h1>
			</div>
		</header>
	<?php endif; ?>

	<div class="article-content container">
		<div class="row">
			<div class="col-md-12">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</article>