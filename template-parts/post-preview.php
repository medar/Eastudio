<?php
$content             = strip_shortcodes( $post->post_content );
$excerpt             = wp_trim_words( $content, $num_words = 20, $more = null );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'blog__item-masonry col-md-6 col-lg-4' ); ?>>
	<div class="img_wrapper">
		<a href="<?php echo  get_permalink(); ?>">
			<picture>
				<?php echo get_the_post_thumbnail(); ?>
			</picture>
		</a>
	</div>
	<p class="article-meta"><span class="article-date"><?php the_date( 'M d, Y, ' ); ?></span>
		<span class="article-author">By <?php the_author_meta( 'display_name' ); ?></span></p>
	<h2 class="post-preview__title"><a href="<?php echo  get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
	<p class="post-preview__excerpt"><?php echo $excerpt; ?></p>
	<a href="<?php the_permalink(); ?>" class="post-preview__link"><span>continue reading</span></a>
</article>