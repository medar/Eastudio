<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 */
?>

<div class="wrapper" id="wrapper-footer">
	<footer>
		<?php echo fw_get_db_settings_option( 'footer' ); ?>
	</footer>

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<a id="back-to-top" href="javascript:void(0)">
	<i class="fa fa-angle-up fa-2x" aria-hidden="true"></i>
</a>

<div class="callback">
	<a class="callback_link popup-with-form" data-toggle="modal" data-target="#contact-modal" href="javascript:void(0)" data-wpel-link="internal">
		<div class="callback_block">
			<div class="callback_a"></div>
			<div class="callback_b"></div>
		</div>
	</a></div>

<!-- Modal -->
<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="contact-modal-Label"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="contact-modal-label">Форма обратной связи</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<?php echo do_shortcode( '[contact-form-7 id="364" title="Форма для модального окна"]' ); ?>
			</div>
			<!--			<div class="modal-footer">-->
			<!--				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
			<!--				<button type="button" class="btn btn-primary">Save changes</button>-->
			<!--			</div>-->
		</div>
	</div>
</div>

<?php wp_footer(); ?>

<script src="https://cdn.jsdelivr.net/parallax.js/1.4.2/parallax.min.js"></script>
</body>
</html>