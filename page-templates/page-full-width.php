<?php
/**
 * Template Name: Full-width
 *
 * Description: Template for Full-width page
 */

get_header();
?>
<main id="main" class="site-main">
	<?php
	while ( have_posts() ) : the_post();
		get_template_part( 'template-parts/content', 'page-full-width' );
	endwhile; // End of the loop.
	?>
</main>

<?php get_footer(); ?>
