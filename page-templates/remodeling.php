<?php
/**
 * Template Name: Remodeling
 *
 * Description: Template for Remodeling page
 */

// TODO: remove this template?

get_header();
?>
<main id="main" class="site-main">
	<h1><?php the_title(); ?></h1>
	<?php
	the_post();
	the_content();
	wp_reset_postdata();
	?>
</main>

<?php get_footer(); ?>
