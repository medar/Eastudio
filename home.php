<?php get_header();
?>

<main id="main" class="site-main">
	<div class="title-img__wrapper">
		<picture>
			<?php echo  get_the_post_thumbnail(get_option( 'page_for_posts' ),'full', array('class' => 'parallax') ); ?>
		</picture>
		<div class="title__wrapper">
			<div class="container"><h1>News</h1></div>
		</div>
	</div>
	<div class="container">
		<div class="blog__items-masonry row">
			<div class="masonry-sizer col-md-6 col-lg-4"></div>
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'template-parts/post-preview' ); ?>
				<?php endwhile;
			endif;
			wp_reset_postdata();
			?>
		</div>
	</div>
</main>

<?php get_footer(); ?>
