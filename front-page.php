<?php get_header(); ?>

	<main id="main" class="site-main">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 p0">
					<div class="front-main-slider__wrapper">
						<div id="front-main-slider" class="owl-carousel">
							<?php
							$slider_img_raw = fw_get_db_settings_option( 'slider-img' );
							$slider_img_ids = array();

							foreach ( $slider_img_raw as $i ) :
								$slider_img_ids[] = $i['attachment_id'];
							endforeach;

							foreach ( $slider_img_ids as $slider ) :
								echo $slider;
								?>
								<div>
									<?php echo wp_get_attachment_image($slider, 'full'); ?>
								</div>

								<?php
							endforeach;
							?>
						</div>
						<div class="front-main-slider__hold">
							<div class="front-main-slider--content">
								<img class="slider__logo animated fadeIn"
									 src="https://i0.wp.com/extrarchstudio.ru/wp-content/uploads/2016/08/logo-novyy.png"
									 alt="logo">
								<h1 class="animated fadeIn animated--delay-1">Дизайн и архитектура</h1>
								<p class="animated--delay-2 animated fadeIn">
									<a href="#contacts-id" class="btn btn--gray btn--full scroll">Консультация<i
												class="fa fa-arrow-right"
												aria-hidden="true"></i></a>
								</p>
								<p class="animated--delay-3 animated fadeIn">
									<a href="#portfolio" class="btn btn--gray btn--full scroll">Портфолио<i
												class="fa fa-arrow-right"
												aria-hidden="true"></i></a>
								</p>
								<p><a href="#second-block" class="scroll animated fadeIn animated--delay-3 scroll-down"><i
												class="fa fa-angle-double-down fa-5x"
												aria-hidden="true"></i></a></p>
								<div class="slider__content--right animated fadeIn animated--delay-3">
									<ul>
										<li>Профессионально</li>
										<li>Индивидуально</li>
										<li>Качественно</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		while ( have_posts() ) : the_post();
			the_content();
		endwhile; // End of the loop.
		?>
	</main>

<?php get_footer();
