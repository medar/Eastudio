<?php
/**
 * Extrarchstudio functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

if ( ! function_exists( 'eastudio_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function eastudio_setup() {
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		/*
		 * This theme uses wp_nav_menu() in one location.
		 */
		register_nav_menus( array(
			'menu-1' => esc_html( 'Primary' ),
		) );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
//		add_image_size( 'post-header-full', 1170, 500 );
		add_image_size( 'post-preview', 300, 300, true );


		// Set up the Wordpress Theme logo feature.
		add_theme_support( 'custom-logo' );

		add_theme_support( 'post-formats', array( 'quote', 'video' ) );
	}
endif;

add_action( 'after_setup_theme', 'eastudio_setup' );

if ( ! function_exists( 'eastudio_scripts' ) ) :
	function eastudio_scripts() {

		wp_enqueue_style(
			'bootstrap-css',
			'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css',
			array(),
			'4.0.0-beta',
			'all'
		);

		wp_enqueue_style(
			'fontawesome',
			'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
			array(),
			'4.7.0',
			'all'
		);

		wp_enqueue_style(
			'owl-carousel',
			'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css',
			array(),
			'2.2.1',
			'all'
		);

		wp_enqueue_style(
			'owl-carousel-theme',
			'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css',
			array( 'owl-carousel' ),
			'2.2.1',
			'all'
		);


		wp_enqueue_style(
			'animate-css',
			'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css',
			array(),
			'3.5.2',
			'all'
		);

		wp_enqueue_style(
			'eastudio-style',
			get_stylesheet_uri(),
			array( 'bootstrap-css', 'fontawesome', 'owl-carousel-theme', 'animate-css' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_script(
			'popper-js',
			'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js',
			array( 'jquery' ),
			'1.11.0',
			true
		);

		wp_enqueue_script(
			'bootstrap-js',
			'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js',
			array( 'popper-js' ),
			'4.0.0-beta',
			true
		);


		wp_enqueue_script(
			'imagesloaded-js',
			'https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js',
			array(),
			'4.1.1',
			true
		);

		wp_enqueue_script(
			'masonry-js',
			'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js',
			array( 'imagesloaded-js' ),
			'1.0.0',
			true
		);

		wp_enqueue_script(
			'readmore-js',
			'https://fastcdn.org/Readmore.js/2.1.0/readmore.min.js',
			array(),
			'2.1.0',
			true
		);

		wp_enqueue_script(
			'owl-carousel-js',
			'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js',
			array( 'jquery' ),
			'2.2.1',
			true
		);

		wp_enqueue_script(
			'waypoints',
			'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js',
			array(),
			'4.0.1',
			true
		);

		wp_enqueue_script(
			'parallax-js',
			get_template_directory_uri() . '/js/libs/parallax.js',
			array(),
			'2.0.2',
			true
		);

		wp_enqueue_script(
			'tagcanvas-js',
			get_template_directory_uri() . '/js/libs/tagcanvas.min.js',
			array(),
			'2.9',
			true
		);

		wp_enqueue_script(
			'eastudio-js',
			get_template_directory_uri() . '/js/theme.js',
			array(
				'bootstrap-js',
				'parallax-js',
				'masonry-js',
				'readmore-js',
				'owl-carousel-js',
				'waypoints',
				'tagcanvas-js'
			),
			'1.0.0',
			true
		);
	}
endif;

add_action( 'wp_enqueue_scripts', 'eastudio_scripts' );

if ( ! function_exists( 'get_the_portfolio_terms' ) ) :
	function get_the_portfolio_terms( $terms = array() ) {
		$ans = '';
		if ( $terms ) :
			foreach ( $terms as $term ) :
				$ans .= $term->name . '/';
			endforeach;
		endif;

		$ans = trim( $ans, '/' );

		return $ans;
	}
endif;

if ( ! function_exists( 'eastudio_portfolio_items' ) ) :
	function eastudio_portfolio_items( $posts_count = 3, $posts_offset = 0, $is_infinite_scroll = false ) {
		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
		$args  = array(
			'posts_per_page'   => $posts_count,
			'paged'            => $paged,
			'post_type'        => 'portfolio',
			'post_status'      => 'publish',
			'orderby'          => 'date',
			'order'            => 'DESC',
			'offset'           => $posts_offset,
			'suppress_filters' => true

		);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) :
			while ( $query->have_posts() ) :
				$query->the_post();
				$post = $query->post;
				setup_postdata( $post );
				$portfolio_item_cats = get_the_terms( $post, 'portfolio_cat' );
				?>
				<div class="portfolio__item--masonry col-sm-6 col-md-4 col-lg-3">
					<div class="portfolio__picture--wrapper--masonry">
						<picture>
							<?php echo get_the_post_thumbnail( $post, 'full' ); ?>
						</picture>
					</div>
					<div class="portfolio__info--wrapper--masonry">
						<a class="portfolio__link--masonry" href="<?php echo get_permalink( $post ); ?>">
							<div class="portfolio__info--masonry">
								<h4><?php echo $post->post_title; ?></h4>
								<p><?php echo get_the_portfolio_terms( $portfolio_item_cats ); ?></p>
							</div>
						</a>
					</div>
				</div>
				<?php
			endwhile;
		endif;
		wp_reset_postdata();
	}
endif;

if ( ! function_exists( 'ajax_portfolio_infinite_scroll_callback' ) ) :

	function ajax_portfolio_infinite_scroll_callback() {
		$offset = intval( $_POST['offset'] );

		eastudio_portfolio_items( 4, $offset, true );
		wp_die();
	}
endif;

add_action( 'wp_ajax_infinite_scroll', 'ajax_portfolio_infinite_scroll_callback' );
add_action( 'wp_ajax_nopriv_infinite_scroll', 'ajax_portfolio_infinite_scroll_callback' );


/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}

add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 *
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array  $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 *
 * @return array Difference between the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

		$urls = array_diff( $urls, array( $emoji_svg_url ) );
	}

	return $urls;
}

// Includes

/**
 * Load custom WordPress nav walker.
 */
require 'inc/bootstrap-wp-navwalker.php';

require 'inc/custom-post-types.php';

function eastudio_security() {
	add_filter( 'the_generator', '__return_empty_string' );
}

eastudio_security();

  function dumper($obj)
  {
    echo
      "<pre>",
        htmlspecialchars(dumperGet($obj)),
      "</pre>";
  }
  // Возвращает строку - дамп значения переменной в древовидной форме
  // (если это массив или объект). В переменной $leftSp хранится
  // строка с пробелами, которая будет выводиться слева от текста.
  function dumperGet(&$obj, $leftSp = "")
  {
    if (is_array($obj)) {
      $type = "Array[".count($obj)."]";
    } elseif (is_object($obj)) {
      $type = "Object";
    } elseif (gettype($obj) == "boolean") {
      return $obj? "true" : "false";
    } else {
      return "\"$obj\"";
    }
    $buf = $type;
    $leftSp .= "    ";
    for (Reset($obj); list($k, $v) = each($obj); ) {
      if ($k === "GLOBALS") continue;
      $buf .= "\n$leftSp$k => ".dumperGet($v, $leftSp);
    }
    return $buf;
  }